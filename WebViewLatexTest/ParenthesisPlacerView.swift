//
//  ParenthesisPlacerView.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 7/18/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class ParenthesisPlacerView : UIView {
    var background: UIImageView!
    var moveLeft: UIButton!
    var moveRight: UIButton!
    var shootLeft: UIButton!
    var shootRight: UIButton!
    var baseFormula: MTMathUILabel!
    var baseFormula2: MTMathUILabel!
    var formulaExtension: MTMathUILabel!
    
    var checkAnswer: UIButton!
//    var swapEdit: UIButton!
    
    var spaceship: UIImageView!
    var workingFormula: MathEditLabel!
    var pointer: UIImageView!
    
    var editingIndex = 0
    var correctAnswer: String!
    var questionBank = NSMutableArray()
    var eraseMode = false
    var points = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        background = UIImageView(frame: CGRect(x: 0,y: 0,width: frame.width,height: frame.height))
        background.image = UIImage(named: "spaceBackground1.png")
        backgroundColor = UIColor.black
        
        moveLeft = UIButton()
        moveLeft.frame = CGRect(x: 0, y: frame.height - 100, width: 100, height: 100)
        moveLeft.setImage(UIImage(named: "leftSpaceButton1.png"), for: .normal)
        moveLeft.addTarget(self, action: #selector(self.moveShipLeft), for: .touchUpInside)
        
        moveRight = UIButton()
        moveRight.frame = CGRect(x: frame.width - 100, y: frame.height - 100, width: 100, height: 100)
        moveRight.setImage(UIImage(named: "rightSpaceButton1.png"), for: .normal)
        moveRight.addTarget(self, action: #selector(self.moveShipRight), for: .touchUpInside)
        
        shootLeft = UIButton()
        shootLeft.frame = CGRect(x: 0, y: frame.height - 175, width: 75, height: 75)
        shootLeft.setImage(UIImage(named: "spaceButtonTemplate1.png"), for: .normal)
        shootLeft.addTarget(self, action: #selector(self.shootLeftParenthesis), for: .touchUpInside)
        
        let leftShootPar = MTMathUILabel();
        leftShootPar.textAlignment = .center
        leftShootPar.isUserInteractionEnabled = false
        leftShootPar.frame = CGRect(x: 0, y: 0, width: shootLeft.frame.width, height: shootLeft.frame.height)
        leftShootPar.latex = "\\left(\\right."
        maximizeFontSize(label: leftShootPar)
        shootLeft.addSubview(leftShootPar)
        
        
        
        shootRight = UIButton()
        shootRight.frame = CGRect(x: frame.width - 75, y: frame.height - 175, width: 75, height: 75)
        shootRight.setImage(UIImage(named: "spaceButtonTemplate1.png"), for: .normal)
        shootRight.addTarget(self, action: #selector(self.shootRightParenthesis), for: .touchUpInside)
        
        let rightShootPar = MTMathUILabel();
        rightShootPar.textAlignment = .center
        rightShootPar.isUserInteractionEnabled = false
        rightShootPar.frame = CGRect(x: 0, y: 0, width: shootRight.frame.width, height: shootRight.frame.height)
        rightShootPar.latex = "\\left.\\right)"
        maximizeFontSize(label: rightShootPar)
        shootRight.addSubview(rightShootPar)
        
        baseFormula = MTMathUILabel()
        baseFormula.frame = CGRect(x: 0, y: 0, width: frame.width / 2, height: 37.5)
        baseFormula.textAlignment = .center
        baseFormula.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.25)
        baseFormula.textColor = UIColor.white
        
        baseFormula2 = MTMathUILabel()
        baseFormula2.frame = CGRect(x: 0, y:37.5, width: frame.width / 2, height: 37.5)
        baseFormula2.textAlignment = .center
        baseFormula2.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.25)
        baseFormula2.textColor = UIColor.white
        
        formulaExtension = MTMathUILabel()
        formulaExtension.frame = CGRect(x: frame.width / 2, y: 0, width: frame.width / 2 * 0.75, height: 75)
        formulaExtension.textAlignment = .center
        formulaExtension.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.25)
        formulaExtension.textColor = UIColor.white
        
        spaceship = UIImageView()
        spaceship.image = UIImage(named: "ship1.png")
        spaceship.frame = CGRect(x: frame.width / 2 - 37, y: frame.height - 75, width: 75, height: 75)
        //spaceship.backgroundColor = UIColor.green
        
        workingFormula = MathEditLabel()
        workingFormula.frame = CGRect(x: frame.width * 0.175, y: 80, width: frame.width * 0.65, height: 100)
        workingFormula.backgroundColor = UIColor(colorLiteralRed: 1, green: 1, blue: 1, alpha: 0.25)
        
        pointer = UIImageView()
        pointer.frame = CGRect(x: 0, y: 80, width: 1, height: frame.height - 80 - spaceship.frame.height / 2)
        pointer.backgroundColor = UIColor.green
        
        
        checkAnswer = UIButton()
        checkAnswer.frame = CGRect(x: formulaExtension.frame.maxX, y: 0, width: frame.width - formulaExtension.frame.maxX, height: 75)
        checkAnswer.setBackgroundImage(UIImage(named: "spaceButtonTemplate1.png"), for: .normal)
        checkAnswer.addTarget(self, action: #selector(self.checkForCorrect), for: .touchUpInside)
        checkAnswer.setImage(UIImage(named: "check.png"), for: .normal)
        
//        swapEdit = UIButton()
//        swapEdit.frame = CGRect(x: formulaExtension.frame.maxX, y: 37.5, width: frame.width - formulaExtension.frame.maxX, height: 37.5)
//        swapEdit.setImage(UIImage(named: "spaceButtonTemplate1.png"), for: .normal)
//        swapEdit.addTarget(self, action: #selector(self.toggleEditMode), for: .touchUpInside)
        
        
        addSubview(background)
        addSubview(workingFormula)
        addSubview(baseFormula)
        addSubview(baseFormula2)
        addSubview(formulaExtension)
        addSubview(pointer)
        addSubview(spaceship)
        addSubview(shootRight)
        addSubview(shootLeft)
        addSubview(moveLeft)
        addSubview(moveRight)
        
        addSubview(checkAnswer)
//        addSubview(swapEdit)
        
        
        
        
        questionBank = readJSONDatabase(db: "placer")
        
        
        
        
        setupQuestion(index: points)
        moveShipToPosition()
        //     .-'
        //'--./ /     _.---.
        //'-,  (__..-`       \
        //   \          .     |
        //    `,.__.   ,__.--/
        //      '._/_.'___.-`

        
        
    }
    func toggleEditMode(){
        eraseMode = !eraseMode
    }
    func setupQuestion(index: Int){
        
        if(index >= questionBank.count) {
            let gov = GameOverFadeView(frame: frame,msg: "You win!")
            addSubview(gov)
            return
        }
        
        let questionDict = questionBank.object(at: index) as! NSDictionary
        let tempSet = (questionDict.value(forKey: "question") as! String).components(separatedBy: "$")
        correctAnswer = questionDict.value(forKey: "answer") as! String
        
        workingFormula.clear()
        for j in tempSet {
            workingFormula.pushLabel(section: j, color: UIColor.green)
        }
        
        baseFormula.latex = "f\\left(x\\right)=" + (questionDict.value(forKey: "fx") as! String)
        baseFormula2.latex = "g\\left(x\\right)=" + (questionDict.value(forKey: "gx") as! String)
        formulaExtension.latex = questionDict.value(forKey: "template") as! String
        editingIndex = 0
        moveShipToPosition()
    }
    func shootLeftParenthesis() {
        
        Sound.play(file: "Laser_Shoot", fileExtension: "caf", numberOfLoops: 0)
        //        workingFormula.insertLabel(section: "\\left.\\right)", color: UIColor.red, index: editingIndex)
        
        let pew = MTMathUILabel()
        pew.frame = spaceship.frame
        pew.textAlignment = .center
        pew.latex = "\\left(\\right."
        pew.textColor = UIColor.red
        addSubview(pew)
        self.pointer.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            
            pew.frame = CGRect(x: pew.frame.minX, y: self.workingFormula.frame.minY,width: pew.frame.width, height: self.workingFormula.frame.height)
            
        }, completion: { finished in
            pew.removeFromSuperview()
            
            self.pointer.backgroundColor = UIColor.green
            self.workingFormula.insertLabel(section: "\\left(\\right.", color: UIColor.red, index: self.editingIndex)
            self.moveShipRight()
            self.moveShipToPosition()
        })
    }
    func shootRightParenthesis() {
        Sound.play(file: "Laser_Shoot", fileExtension: "caf", numberOfLoops: 0)
//        workingFormula.insertLabel(section: "\\left.\\right)", color: UIColor.red, index: editingIndex)
        
        let pew = MTMathUILabel()
        pew.frame = spaceship.frame
        pew.textAlignment = .center
        pew.latex = "\\left.\\right)"
        pew.textColor = UIColor.red
        addSubview(pew)
        self.pointer.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveLinear, animations: {
            
            pew.frame = CGRect(x: pew.frame.minX, y: self.workingFormula.frame.minY,width: pew.frame.width, height: self.workingFormula.frame.height)
            
        }, completion: { finished in
            pew.removeFromSuperview()
            
            self.pointer.backgroundColor = UIColor.green
            self.workingFormula.insertLabel(section: "\\left.\\right)", color: UIColor.red, index: self.editingIndex)
            self.moveShipRight()
            self.moveShipToPosition()
        })
    }
    func checkForCorrect(){
        let d = workingFormula.getFullLatex()
        let check = correctAnswer.replacingOccurrences(of: "\\\\", with: "\\")
        if(d == check){
            points += 1
            setupQuestion(index: points)
            Sound.play(file: "success1.caf")
        } else {
            Sound.play(file: "error.caf")
            setupQuestion(index: points)
        }
        
    }
    func moveShipLeft() {
        editingIndex = max(0, editingIndex - 1)
        moveShipToPosition()
    }
    func moveShipRight() {
        editingIndex = min(workingFormula.labels.count, editingIndex + 1)
        moveShipToPosition()
    }
    func moveShipToPosition() {
        if(workingFormula.labels.count == 0) {
            //empty working formula
            spaceship.frame = CGRect(x: frame.width / 2 - 37, y: frame.height - 75, width: 75, height: 75)
        } else if(editingIndex == workingFormula.labels.count) {
            //editing position is at the end
            spaceship.frame = CGRect(x: workingFormula.frame.minX + workingFormula.getLabelAt(index: workingFormula.labels.count - 1).frame.maxX - 37, y: frame.height - 75, width: 75, height: 75)
        } else {
            spaceship.frame = CGRect(x: workingFormula.frame.minX + workingFormula.getLabelAt(index: editingIndex).frame.minX - 37, y: frame.height - 75, width: 75, height: 75)
            
        }
        pointer.frame = CGRect(x: spaceship.frame.midX - pointer.frame.width / 2, y: pointer.frame.minY, width: pointer.frame.width, height: pointer.frame.height)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
