//
//  Rabbit.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/20/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class Rabbit: UIImageView {
    let hopImage = UIImage(named: "hop1.png")
    let notHopImage = UIImage(named: "rabbit.png")
    
    let bite1 = UIImage(named: "chomp1-1.png")
    let bite2 = UIImage(named: "chomp2-1.png")
    
    var owner: RabbitView!
    
    var carrot: Carrot!
    var speed: CGFloat!
    var formula: MTMathUILabel!
    var targetReached = false
    var target: CGPoint!
    var isHopping = false
    
    init(owner: RabbitView) {
        super.init(image: UIImage(named: "rabbit.png"))
        self.owner = owner;
        
    }
    required init?(coder: NSCoder){
        super.init(coder: coder)
    }
    
    public func update(delta: Double) {
        
        let target = CGPoint(x: carrot.frame.minX, y: carrot.center.y)
        
        let dfx = target.x - center.x;
        let dfy = target.y - center.y;
        //calculate the angle to the target
        let angle = atan2(dfy, dfx);
        //calculate the cos and sin of the angle relative to the speed and time passed
        let dx = cos(angle) * speed * CGFloat(delta)
        let dy = sin(angle) * speed * CGFloat(delta)
        
        center.x += dx;
        center.y += dy;
        
        let distanceMoved = sqrt(pow(dx, 2) + pow(dy, 2))
        
        let distanceToTarget = sqrt(pow(dfx, 2) + pow(dfy, 2))
        
        //set to false if either of the axis aren't close enough
        targetReached = true
        //if the target is close enough, just snap to it
        if(distanceMoved >= distanceToTarget) {
            center.x = target.x
            center.y = target.y
        } else {
            targetReached = false
            Sound.stop(file: "rabbitCarrot.caf", fileExtension: nil)
        }
        
        owner.zOrder(rabbit: self)
        
        //move the formula along with the rabbit
        
        if(owner.subviews.contains(formula)) {
            //do damage to the carrot
            var curHealth = carrot.health
            carrot.health = curHealth - delta
            
            
            curHealth = curHealth / carrot.carrotHealthTimer
            
            if(curHealth < 0) {
                Sound.stop(file: "rabbitCarrot.caf", fileExtension: nil)
                owner.findNewTarget(rab: self)
            }
            
            
            
        } else if(targetReached){
            if(SettingsManager.soundOn){
                Sound.play(file: "rabbitCarrot.caf", fileExtension: nil, numberOfLoops: -1)
            }
            formula.frame = CGRect(x: owner.gardenBG.frame.width / 2 - formula.frame.width / 2, y: 0, width: formula.frame.width, height: formula.frame.height);
            owner.addSubview(formula)
            formula.isHidden = false
            owner.questionBox.frame = owner.incorrect.frame
            
        }

    }
    public func setTarget(carrot: Carrot) {
        self.carrot = carrot
        target = CGPoint(x: carrot.frame.minX, y: carrot.center.y)
    }
    public func animate() {

        if !targetReached {
            
            if isHopping {
                image = notHopImage
                isHopping = false
            } else {
                image = hopImage
                isHopping = true
            }
        } else {
            
            if isHopping {
                image = bite1
                isHopping = false
            } else {
                image = bite2
                isHopping = true
            }
        }
        
        if(target.x < center.x) {
            image = getFlippedImage(image: image!)
        }
    }
}
