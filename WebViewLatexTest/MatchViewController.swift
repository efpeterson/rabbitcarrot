//
//  MatchViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/7/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit
import iosMath

class MatchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    //Outlet for the collection view connections
    @IBOutlet var gameCollectionView: UICollectionView!
    
    //Outlet for the center constraint that is used to animate that game over screen
    @IBOutlet var centerGameOverConstraint: NSLayoutConstraint!
    
    //Initialize a new card deck
    var cardDeck = CardDeck()
    
    //Selection round where there are 2 per attempt
    var selectionRound: Int = 0
    
    //Array used to store the position of each of the two cards
    var cards:[Int] = [0,0]
    
    //Array of indicies
    var selectionsIndex:[Int] = []
    
    //Array of cells
    var cells:[MatchCollectionViewCell] = []
    
    //cells counter to determine the end of the game
    var cellsCounter = -1
    
  

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //randomize the deck
        cardDeck.randomizeCards()
        
        //layout of each cell
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20, 20, 20, 20)
        
        //background of the collection view
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"matchBackground.png")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
       
        gameCollectionView.collectionViewLayout = layout
        gameCollectionView.backgroundView = imageView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    
   //return the number of cells in the view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5*4
    }
    
    
    //defines the cell properties at initialization
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MatchCollectionViewCell
        cell.backImageView.image = UIImage(named: cardDeck.getCardBack())
        return cell
    }
    
    //sets the cells in the defined positions
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: view.frame.width/6, height: view.frame.height/5)
    }
    
    //Method that calls when a cell is selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = gameCollectionView.cellForItem(at: indexPath) as! MatchCollectionViewCell
       
        //stores the first two selected cells
        if selectionRound < 2 && cellsCounter < 19{
            UIView.transition(with: cell.backImageView, duration: 0.5, options: .transitionFlipFromLeft , animations: nil, completion: nil)
            cell.backImageView.image = UIImage(named: cardDeck.getFormula(index: indexPath.row))
            cards[selectionRound] = cardDeck.cardValue(index: indexPath.row)
            selectionsIndex.append(indexPath.row)
            cells.append(cell)
            cellsCounter += 1
            selectionRound += 1
            print(Sound.enabled)
            print(Sound.play(file: "Splash_Dust.caf"))
           
            //Determines if there is a match, if no match the two cards flip back over
            if  selectionRound == 2 && !cardDeck.isMatched(firstCard: cards[0], secondCard: cards[1]) {
                
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0){
                UIView.transition(with: self.cells[self.cellsCounter - 1].backImageView, duration: 0.5, options: .transitionFlipFromLeft , animations: nil,completion: nil)
                self.cells[self.cellsCounter - 1].backImageView.image = UIImage(named: self.cardDeck.getCardBack())
                UIView.transition(with: self.cells[self.cellsCounter].backImageView, duration: 0.5, options: .transitionFlipFromLeft , animations: nil, completion: nil)
                self.cells[self.cellsCounter].backImageView.image = UIImage(named: self.cardDeck.getCardBack())
                self.selectionsIndex.removeAll()
                self.selectionRound = 0
                    print ("misMatch" + "\(self.cellsCounter)")
                    print ("misMatch" + "\(self.cellsCounter - 1)")
                self.cells.remove(at: self.cellsCounter)
                self.cells.remove(at: self.cellsCounter - 1)
                self.cellsCounter -= 2
                Sound.play(file: "Splash_Dust.caf")
                }
            }
            
            //If matched the cells are disabled and a new selection round is started
            if cardDeck.isMatched(firstCard: cards[0], secondCard: cards[1]){
                cells[cellsCounter - 1].isUserInteractionEnabled = false
                cells[cellsCounter].isUserInteractionEnabled = false
                selectionRound = 0
                
                Sound.play(file: "success1.caf")

            }
            
            //After all cells have been selected and matched the game over view is called
            if cellsCounter == 19{
                centerGameOverConstraint.constant = 0
                 UIView.animate(withDuration: 0.3, animations: {
                 self.view.layoutIfNeeded()
                 self.gameCollectionView.alpha = 0.3
                 })
            }

          
        }
        
    
        
        
    }
    //Back button in the game over view
    @IBAction func backButtonAction(_ sender: Any) {
        guard (navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
    }
    
}

