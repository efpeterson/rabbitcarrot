//
//  Stack.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 6/28/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation

class Stack<T> {
    
    private var top: Int
    private var items: [T]
    var size: Int
    
    init(){
        top = -1
        items = [T]()
        size = 20
    }
    
    init(size: Int){
        top = -1
        items = [T]()
        self.size = size
    }
    
    func push(item: T) -> Bool {
        if !isFull(){
            items.append(item)
            top += 1
            return true
        }
        print("Stack is full")
        return false
    }
    
    func pop() -> T? {
        if !isEmpty(){
            top -= 1
            return items.removeLast()
        }
        print("Stack is empty")
        return nil
    }
    
    func peek() -> T? {
        if !isEmpty(){
            return items.last
        }
        return nil
    }
    
    func isFull() -> Bool{
        return top == (size - 1)
    }
    
    func isEmpty() -> Bool {
        return top == -1
    }
    
    func count() -> Int {
        return(top + 1)
            
        }
    func clear()  {
        items.removeAll()
    }
}
    
    


