//
//  MathEditLabel.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 7/18/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class MathEditLabel : UIView {
    let labels = NSMutableArray()
    func pushLabel(section: String, color: UIColor) -> MTMathUILabel{
        return insertLabel(section: section, color: color, index: labels.count)
    }
    func insertLabel(section: String, color: UIColor, index: Int) -> MTMathUILabel {
        let label = MTMathUILabel()
        label.latex = section
        label.textColor = color
        
        if(labels.count != 0) {
            labels.insert(label, at: index)
        } else {
            labels.add(label)
        }
        addSubview(label)
        
        resizeLabels()
        return label
    }
    func clear() {
        for l in labels {
            let lab = l as! MTMathUILabel
            lab.removeFromSuperview()
        }
        labels.removeAllObjects()
    }
    func resizeLabels(){
        var newWidth = 0 as CGFloat
        for l in labels {
            let lab = l as! MTMathUILabel
            let sizeToFit = lab.sizeThatFits(.zero)
            newWidth += sizeToFit.width
        }
        
        let relativeXOrigin = frame.width / 2 - newWidth / 2
        
        var lastX = relativeXOrigin
        
        for l in labels {
            let lab = l as! MTMathUILabel
            let sizeToFit = lab.sizeThatFits(.zero)
            lab.frame = CGRect(x: lastX, y: 0, width: sizeToFit.width ,height: frame.height)
            lastX += sizeToFit.width
        }
        
    }
    func getLabelAt(index: Int) -> MTMathUILabel {
        return labels.object(at: index) as! MTMathUILabel
    }
    func isLeftParenthesisAt(index: Int) -> Bool {
        return (getLabelAt(index: index).latex?.contains("("))!
    }
    func isRightParenthesisAt(index: Int)-> Bool {
        return (getLabelAt(index: index).latex?.contains(")"))!
    }
    func getFullLatex() -> String {
        var str = ""
        for l in labels {
            let lab = l as! MTMathUILabel
            str.append(lab.latex!.replacingOccurrences(of: "\\left.", with: "").replacingOccurrences(of: "\\right.", with: ""))
        }
        return str
    }
}
