//
//  RabbitView.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/27/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import iosMath
import UIKit

class RabbitView: UIView {

    var timer: Timer? //main logic timer for movment
    var upsTimer: Timer? //timer to monitor updates per second
    var hopAnimationTimer: Timer? //timer to show hopping animation
    var counter  = 0 //counts updates per second for timer
    var movingFormula: MTMathUILabel? //the formula being dragged. Is nil when nothing is being dragged
    var formulas: NSMutableArray! //an array that stores the formula
    
    var carrots: NSMutableArray! //an array that stores the carrot UIImages
    
    
    var rabbits: NSMutableArray! //an array that stores the rabbits on the screen
    var incorrect: MTMathUILabel! //temporary storage of the formula view on the rabbit AKA the question
    var lastTime = Int64(-1) //variable used in determining the timing for each loop
    var gardenBG: UIImageView! //The garden background image
    
    var scoreCounter: UILabel! // counts rabbits left to destroy
    var scoreDisplay: UIImageView! //background image for the score counter
    var lastCarrotIndex: Int! // last carrot index in the subview list, to add rabbits after it
    var questionBox: UIImageView! // the background image that shows the formula at the top of the screen
    
    var questionBank: NSMutableArray! //database of all the questions
    let rabbitSpeed = Float(75)
    let rows = 3
    let columns = 3;
    var rowYValues : [Carrot] = []
    
    var formulasAnswered: [Int] = []
    var currentQuestionIndex = -1
    
    var gameOverLabel: UILabel!
    var gameOverButton: UIButton!
    let settingImage = UIImage(named: "settings1x.png")
    
    var controller: ViewController!
    
    init(frame: CGRect, controller: ViewController) {
        super.init(frame: frame)
        //create garden background image
        gardenBG = UIImageView()
        gardenBG.image = UIImage(named: "scene.png")
        gardenBG.frame = CGRect(x: 0, y: 0, width: cgScale(467), height: self.frame.height)
        
        self.addSubview(gardenBG)
        
        
        
        //initialize arrays
        formulas = NSMutableArray()
        rabbits = NSMutableArray()
        carrots = NSMutableArray()
        questionBank = NSMutableArray()
        let yOffset = scale(130)
        let widthGap = Int(gardenBG.frame.width) / (columns + 1)
        let yGap = (Int(gardenBG.frame.height) - yOffset) / (rows + 1)
        let imageWidth = scale( 77)
        
        print("widthGap \(widthGap) bgWidth \(gardenBG.frame.width) Imagewidth \(imageWidth)")
        let image = UIImage(named: "carrot.png")
        var lastX = -(imageWidth / 2)
        var lastY = yOffset - yGap
        //create 9 carrots in a 3 by 3 pattern
        for i in 0...(rows * columns - 1) {
            let carrot = Carrot(owner: self)
            carrot.image = image
            
            let x = lastX + widthGap
            lastX = x
            var y = lastY
            if(i % columns == 0) {
                //if first in row
                y = lastY + yGap
                lastY = y
            }
            
            
            carrot.frame = CGRect(x: x, y: y, width: imageWidth, height: imageWidth)
            carrot.resetHealthTimer() // time measured in seconds
            carrots.add(carrot)
            self.addSubview(carrot)
            if(i % columns == 0) {
                rowYValues.append(carrot)
                
                
            }
            if(i % columns == columns - 1) {
                lastX = -(imageWidth / 2)
            }
            
        }
        lastCarrotIndex = self.subviews.count
        //create toolbox background image
        let toolbox = UIImageView()
        toolbox.image = UIImage(named: "toolbox.png")
        toolbox.frame = CGRect(x: gardenBG.frame.width, y: 0, width: self.frame.width - gardenBG.frame.width, height: self.frame.height)
        
        self.addSubview(toolbox)
        
        let menuButton = UIButton()
        menuButton.setTitle("settings", for: .normal)
        menuButton.frame = CGRect(x: 0, y: super.bounds.maxY - 30, width: 30, height: 30)
        menuButton.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        menuButton.setImage(settingImage, for: .normal)
        self.addSubview(menuButton)
        
        questionBank = readJSONDatabase(db: "questionBank1")
        //load a question
        loadRandomQuestion()
        
        //create the image to fit behind the incorrect formula
        questionBox = UIImageView()
        questionBox.image = UIImage(named: "questionBox1.png")
        self.addSubview(questionBox)
        
        //initialize the score display parts
        scoreDisplay = UIImageView()
        scoreDisplay.image = UIImage(named: "scoreCounter1.png")
        scoreDisplay.frame = CGRect(x: 0, y: 0, width: scale( 75), height: scale( 25))
        self.addSubview(scoreDisplay)
        
        scoreCounter = UILabel()
        scoreCounter.text = "0"
        scoreCounter.textColor = UIColor.black
        scoreCounter.frame = CGRect(x: scale(10), y: 0, width: scale( 50), height: scale( 25))
        self.addSubview(scoreCounter)
        
        
        gameOverLabel = UILabel()
        gameOverLabel.text = "GAME OVER"
        gameOverLabel.textColor = UIColor.white
        gameOverLabel.font = gameOverLabel.font.withSize(cgScale(20))
        
        gameOverLabel.sizeToFit()
        gameOverLabel.center = CGPoint(x: gardenBG.frame.width / 2, y: self.frame.height / 2)
        
        gameOverButton = UIButton(frame: CGRect(x: gardenBG.frame.width - cgScale( 100), y: self.frame.height / 2 + cgScale( 50), width: cgScale( 200),height:cgScale( 45)))
        gameOverButton.center = gameOverLabel.center
        gameOverButton.center.y = gameOverButton.center.y - cgScale( 50)
        gameOverButton.setBackgroundImage(UIImage(named: "menuButton1.png"), for: UIControlState.normal)
        
        gameOverButton.setTitle("Tap to Continue", for: UIControlState.normal)
        gameOverButton.addTarget(self, action: #selector(gameOverButtonTapped), for: .touchUpInside)
        
        self.controller = controller
        
        //pick random target
        
        //spawn the rabbit
        spawnRabbit(formula: incorrect, target: getRandomHealthyCarrot(), speed: rabbitSpeed)
        
        //create and start the timers
        timer = Timer.scheduledTimer(timeInterval: 0.01666666, target: self, selector: #selector(self.runLoop), userInfo: nil, repeats: true)
        upsTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkUPS), userInfo: nil, repeats: true)
        hopAnimationTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.animateRabbit), userInfo: nil, repeats: true)
        

    }
    func gameOverButtonTapped() {
        controller.backButtonActionButton(self)
        Sound.stop(file: "rabbitCarrot.caf")
    }
    
    /*
     
     
     */
    func zOrder(rabbit: Rabbit) {
        rabbit.removeFromSuperview()
        var didReadd = false
        for i in 0...(rowYValues.count - 1) {
            if(rabbit.frame.maxY < rowYValues[i].frame.maxY) {
                
                //                self.insertSubview(rabbit, at: 1 + (columns * (i)))
                self.insertSubview(rabbit, belowSubview: rowYValues[i])
                didReadd = true
                // print("picked z = " + String(i))
                break;
            }
        }
        if(!didReadd) {
            self.insertSubview(rabbit, at: lastCarrotIndex)
        }
        
    }
    /*
     
     
     */
    func getRandomHealthyCarrot() -> Carrot {
        var targetIndex = 0
        var targetCarrot: Carrot!
        var tries: [Carrot] = []
        repeat {
            targetIndex = getRandom(range: carrots.count)
            targetCarrot = carrots.object(at: targetIndex) as! Carrot
            if(!tries.contains(targetCarrot)){
                tries.append(targetCarrot)
            }
        } while((targetCarrot.health) < 0 && tries.count != carrots.count)
        if(targetCarrot.health <= 0){
            timer?.invalidate()
            hopAnimationTimer?.invalidate()
//            self.addSubview(gameOverLabel)
//            self.addSubview(gameOverButton)
            addSubview(GameOverFadeView(frame: frame, msg: "Game Over"))
        }
        return targetCarrot
    }
    /*
     
     
     
     */
    func animateRabbit(){
        
        
        for r in rabbits {
            
            (r as! Rabbit).animate()
        }
    }
    /*
     picks a random question from the question bank and loads all the formulaviews for it
     
     */
    func loadRandomQuestion() {
        for f in formulas {
            let formula = f as! MTMathUILabel
            formula.removeFromSuperview()
        }
        if incorrect != nil {
            incorrect.removeFromSuperview()
        }
        formulas.removeAllObjects()
        
        if formulasAnswered.count == questionBank.count {
            //all questions have been answered correctly
            
//            gameOverLabel.text = "You win!"
//            gameOverLabel.sizeToFit()
//            gameOverLabel.center = CGPoint(x: gardenBG.frame.width / 2, y: self.frame.height / 2)
//            timer?.invalidate()
//            hopAnimationTimer?.invalidate()
//            self.addSubview(gameOverLabel)
//            self.addSubview(gameOverButton)
//            
//            return
            addSubview(GameOverFadeView(frame: frame, msg: "You Win!"))
            return
            
        }
        
        var questionIndex = -1
        repeat {
            questionIndex = getRandom(range: questionBank.count)
            
        } while(hasAnsweredQuestion(index: questionIndex))
        
        
        let questionDict = questionBank.object(at: questionIndex) as! NSDictionary
        currentQuestionIndex = questionIndex
        
        //calculate how large the button can be
        let max = questionDict.count - 3;
        let buttonHeight = CGFloat(self.frame.height / CGFloat(max + 1));
        //create buttons for each of the different listed formulas
        for i in 0...max{
            let formula = (questionDict.value(forKey: "answer" + String(i + 1)) as! String)
            let isAnswer = (questionDict.value(forKey: "correctAnswer") as! String)  == (("answer" + String(i + 1)))
            let f = createFormulaView(x: self.frame.width - cgScale( 100), y: CGFloat( buttonHeight / 2 + (buttonHeight * CGFloat(i))), formula: formula, width: cgScale( 190), height: buttonHeight, textColor: UIColor.black, isAnswer: isAnswer)
            formulas.add(f)
            self.addSubview(f)
        }
        
        let question = questionDict.value(forKey: "question") as! String
        //create the incorrect formulaView to connect to the rabbit
        incorrect = createFormulaView(x: 0, y: 0, formula: question, width: cgScale( 200), height: cgScale( 50), textColor: UIColor.red,isAnswer: false)
        unifyFormulaFontSizes(formulas: formulas)
    }
    func hasAnsweredQuestion(index: Int) -> Bool{
        for j in formulasAnswered {
            if j == index {
                return true
            }
        }
        return false;
    }
    /*
     Dragging logic, if movingFormula is not null, move that UIWebView to the current touch location
     */
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first{
            if let toMove = movingFormula {
                let touchLocation = touch.location(in: self)
                toMove.center.y = touchLocation.y
                toMove.center.x = touchLocation.x
            }
        }
    }
    /*
     Handles the beginning of touch events, detects if the touch is inside any formula view and if so, sets them to the movingFormula variable for dragging logic
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let touchLocation = touch.location(in: self)
            print(touchLocation)
            for formula in formulas{
                let formulaView = formula as! MTMathUILabel
                if(formulaView.frame.contains(touchLocation)){
                    self.movingFormula = formulaView;
                    movingFormula?.backgroundColor = UIColor(colorLiteralRed: 0.8, green: 0.8, blue: 1, alpha: 0.5)
                    break;
                }
            }
        }
    }
    func findNewTarget(rab: Rabbit) {
        loadRandomQuestion()
        let newTarget = getRandomHealthyCarrot()
        rab.formula = incorrect
        rab.setTarget(carrot: newTarget)
    }
    /*
     Detects whether a touch release point of dragging a formula is within the bounds of a rabbit
     */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (movingFormula != nil) {
            //Reset the answer's formula view
            let initalPosition = movingFormula?.layer.value(forKey: "initialPosition") as! CGPoint
            movingFormula?.center = initalPosition
            movingFormula?.backgroundColor = UIColor.clear
            
            let touchLoc = touches.first?.location(in: self)
            for rab in rabbits {
                let r = rab as! Rabbit
                
                //first check to make sure the rabbit is actually at its position
                if (r.targetReached) {
                    
                    
                    //check to see if the correct answer was dragged into the bounds of the rabbit
                    if r.frame.contains(touchLoc!) {
                        //check to see if the player has dragged the correct answer
                        if(movingFormula?.layer.value(forKey: "isAnswer") as! Bool){
                            //increment the score
                            if(SettingsManager.soundOn) {
                                Sound.play(file: "success.caf")
                            }
                            
                            let se = SmokeEffect()
                            se.frame = r.frame
                            self.addSubview(se)
                            
                            
                            
                            scoreCounter.text = String(Int(scoreCounter.text!)! + 1)
                            
                            removeRabbit(rab: r)
                            formulasAnswered.append(currentQuestionIndex)
                            loadRandomQuestion()
                            
                            //spawn a new rabbit
                            spawnRabbit(formula: incorrect, target: getRandomHealthyCarrot(), speed: rabbitSpeed)
                        } else {
                            //rabbit eats carrot and chooses new target
                            if(SettingsManager.soundOn) {
                                Sound.play(file: "error.caf")
                            }
                            let carrot = r.carrot
                            carrot?.health = -1.0
                            
                            findNewTarget(rab: r)
                        }
                    }
                }
            }
            //clear the moving formula
            movingFormula = nil
        }
    }
    //loop that is run every 1/60th of a second
    func runLoop(){
        if lastTime == -1 {
            lastTime = getCurrentMillis()
        }
        //calculate time passed
        let ct = getCurrentMillis()
        let delta = Double(ct - lastTime) / 1000.0;
        
        
        lastTime = ct;
        counter += 1
        //print(delta)
        
        
        //cycle through the rabbits to update each one
        for r in rabbits {
            let rab = r as! Rabbit
            rab.update(delta: delta)
        }
        for c in carrots {
            let car = c as! Carrot
            car.update(delta: delta)
        }
    }
    //Spawns a randomly placed rabbit
    func spawnRabbit(formula: MTMathUILabel, target: Carrot, speed: Float){
        let rab = Rabbit(owner: self)
        
        //rabbit should spawn in random location
        let onX = randomBool();
        let aboveOther = randomBool();
        if onX {
            let range = self.frame.width - cgScale( 100);
            rab.center.x = CGFloat(arc4random_uniform(UInt32(range)))
            rab.center.y = (self.frame.height + cgScale( 40))
        } else {
            let range = self.frame.height - cgScale( 143);
            rab.center.y = cgScale(143) + CGFloat(arc4random_uniform(UInt32(range)))
            rab.center.x = (aboveOther ? cgScale( -40) : self.frame.width - cgScale( 200) + cgScale( 40))
        }
        //set values for later to use in the udpate code
        
        
        rab.formula = formula;
        rab.setTarget(carrot: target)
        rab.speed = CGFloat(speed);
        
        
        
        rab.isHidden = false;
        rab.frame = CGRect(x: rab.center.x, y: rab.center.y, width: cgScale( 79), height: cgScale( 79))
        
        if(target.frame.minX < rab.center.x) {
            rab.image = getFlippedImage(image: UIImage(named: "rabbit.png")!)
        }
        
        //place the rabbit into the subview
        self.insertSubview(rab, at: lastCarrotIndex);
        rabbits.add(rab)
        
    }
    //Removes the rabbit and all of its children elements, like the formula view
    func removeRabbit(rab: Rabbit){
        rabbits.remove(rab)
        rab.removeFromSuperview()
        (rab.formula).removeFromSuperview()
    }
    func checkUPS(){
        //print(counter)
        counter = 0
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
