//
//  CommonMistakesCollectionViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/18/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit



class CommonMistakesCollectionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //viewSelection is used to store an integer that gets sent to the next view controller to determine 
    //which view to load
    var viewSelection = Int()
    
    //Outlet connection to the collection view
    @IBOutlet var commonMistakeCollectionView: UICollectionView!
    
    //Image and label arrays for each collection cell
    var imageArray = [UIImage(named: "rabbitGamePic"), UIImage(named: "dollGamePic"), UIImage(named: "matchGamePic"), UIImage(named: "foundationalReviewGamePic"), UIImage(named: "fractionGamePic"), UIImage(named:"spaceGamePic"), UIImage(named: "collectionBack")]
    
    var labelArray = ["Chain Rule", "Composite Functions", "Derivative Memorization", "Foundational Review", "Fraction Subtraction", "Parenthesis Placement", "Back"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //layout for each cell
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(80, 20, 50, 50)
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 50
        
        //background image of the collection view
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"commonMistakeBackground")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        
        commonMistakeCollectionView.collectionViewLayout = layout
        commonMistakeCollectionView.backgroundView = imageView

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //returns the number of cells in the collection view
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    
    //returns the cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CommonMistakeViewCell
        cell.mistakePracticeImage.image = imageArray[indexPath.row]
        cell.mistakePracticeLabel.text = labelArray[indexPath.row]
        cell.mistakePracticeImage.layer.cornerRadius = 5
        cell.mistakePracticeImage.layer.borderWidth = 1
        //applyPlainShadow(view: cell.contentView)
        cell.mistakePracticeImage.layer.masksToBounds = true
        
        
        
        return cell
    }
    
    //returns the layout so that the cells are all in one row horizontally
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width/3, height: view.frame.height/2)
    }
    
    //controls what happens when a cell is selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = commonMistakeCollectionView.cellForItem(at: indexPath) as! CommonMistakeViewCell
        
        //sets viewSelection equal to the index of the array of the selected cell plus 1 for the next view
        viewSelection = indexPath.row + 1
        
        //condition to determine if the back cell was selected
        if(viewSelection < imageArray.count){
            performSegue(withIdentifier: "mistake1", sender: cell)
        }else{
            guard (navigationController?.popViewController(animated: true)) != nil
                else {
                    print("No Navigation Controller")
                    return
            }
        }
        
        
    }
    
    //sends the viewSelection value to MistakePracticeViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "mistake1"){
            let controller = segue.destination as! MistakePracticeViewController
            controller.selection = viewSelection
        }
    }
    
   
    //Shadow effect for the cells
    func applyPlainShadow(view: UIView) {
        let layer = view.layer
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 10)
        layer.shadowOpacity = 0.4
        layer.shadowRadius = 5
    }
    

}
