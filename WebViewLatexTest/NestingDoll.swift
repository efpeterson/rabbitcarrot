//
//  NestingDoll.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/28/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import iosMath
import UIKit

class NestingDoll: UIImageView  {
    var top: UIImageView!
    var bottom: UIImageView!
    public var formula: MTMathUILabel?
    public var index: Int?
    
    override init(frame: CGRect){
        
        super.init(frame: frame)
        backgroundColor = UIColor.clear
        top = UIImageView()
        bottom = UIImageView()
        
        top.image = UIImage(named: "nestingTop1.png")
        bottom.image = UIImage(named: "nestingBottom1.png")
        
        let width = frame.width
        
        top.frame = CGRect(x: 0, y: -frame.minY - width, width: width, height: width)
        bottom.frame = CGRect(x: 0, y: frame.maxY * 2, width: width, height: width)
        
        
        addSubview(top)
        addSubview(bottom)
        //animateClose(callback: nil)
    }
    public func copyDataTo(newDoll: NestingDoll){
        newDoll.setFormula(form: (formula?.latex)!)
        newDoll.formula?.textColor = (formula?.textColor)!
        newDoll.index = index
    }
    public func animateClose(_ callback:(() -> ())?) {
        UIView.animate(withDuration: 0.7, delay: 0.2, options: .curveEaseOut, animations: {
            
            
            var topFrame = self.top.frame
            topFrame.origin.y += self.frame.height / 2 + self.frame.minY
            
            
            var bottomFrame = self.bottom.frame
            bottomFrame.origin.y = self.frame.height / 2
            
            self.top.frame = topFrame
            self.bottom.frame = bottomFrame
        }, completion: { finished in
            if (callback != nil) {
                callback?()
            }
        })
    }
    public func animateOpen(_ callback:(() -> ())?) {
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseOut, animations: {
            
            
            var topFrame = self.top.frame
            topFrame.origin.y -= self.top.frame.height
            
            
            var bottomFrame = self.bottom.frame
           // bottomFrame.origin.y += self.bottom.frame.height
            
            var formulaFrame = self.formula?.frame
            formulaFrame?.origin.y = (CGFloat(self.index! ) * ((self.formula)?.frame.height)!)
            formulaFrame?.origin.x -= self.frame.minX
            
            self.top.frame = topFrame
            self.bottom.frame = bottomFrame
            self.formula?.frame  = formulaFrame!
            self.formula?.textColor = UIColor.black
        }, completion: { finished in
            UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseOut, animations: {
                
                
                self.setToDisplayPosition()
            }, completion: { finished in
                if(callback != nil) {
                    callback?()
                }
            })
        })
    }
    func getXDisplayPosition() -> CGFloat{
        if(index! == 0) {
            return cgScale(20)
        }
        var spacing: CGFloat = 0;
        for i in 0...(index! - 1) {
            spacing += cgScale(60) * CGFloat(pow(1.1, i + 2).doubleValue)
        }
        return cgScale(20) + spacing
    }
    public func setToDisplayPosition(){
        var topFrame = self.top.frame
        topFrame.origin.y = self.frame.height - self.top.frame.height
        topFrame.origin.x = -self.frame.minX + getXDisplayPosition()
        
        var bottomFrame = self.bottom.frame
        bottomFrame.origin.y = self.frame.height
        bottomFrame.origin.x = topFrame.origin.x
        
        var formulaFrame = self.formula?.frame
        formulaFrame?.origin.y = bottomFrame.maxY - cgScale(50)
        formulaFrame?.origin.x = topFrame.origin.x
        
        self.top.frame = topFrame
        self.bottom.frame = bottomFrame
        self.formula?.frame  = formulaFrame!
    }
    public func setFormula(form: String) {
        
        formula = createFormulaView(x: frame.width / 2, y: frame.height / 2, formula: form, width: frame.width * 0.9, height: 50, textColor: UIColor.white, isAnswer: false)
        
        insertSubview(self.formula!, aboveSubview: bottom)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
