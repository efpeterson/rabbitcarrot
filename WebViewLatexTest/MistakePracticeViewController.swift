//
//  MistakePracticeViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 6/13/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit
import WebKit

class MistakePracticeViewController: UIViewController {

    var selection = Int()
    var practiceGame = String()
    var webview: WKWebView!
    @IBOutlet var testLabel: UILabel!
    let size = 260
    
    
    
    
    override func viewDidLoad() {
         super.viewDidLoad()
        
        //Stuff for webview.  May not use
        let screenWidth = self.view.frame.size.width
        webview = WKWebView(frame: CGRect(x: Int((screenWidth / 2) - 130), y: 30, width: size, height: size))
        self.view.addSubview(webview)
        getVideo(videoCode: "qmPq00jelpc")
     
        //switch to change the label.  value passed from previous controller
        switch(selection){
        case 1:
            testLabel.text = "Stuff leading up to rabbit game"
            break
        case 2:
            testLabel.text = "Stuff leading up to doll game"
            break
        case 3:
            testLabel.text = "Stuff leading up to match game"
        default:
            break
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    //based on selection from previous controller, the practice game will be defined
    @IBAction func practiceButtonAction(_ sender: Any) {
        Sound.play(file: "buttonclick.caf")
        switch(selection){
        case 1: practiceGame = "RabbitGame"
            performSegue(withIdentifier: "rabbit", sender: self)
            break
        case 2: practiceGame = "DollGame"
            performSegue(withIdentifier: "rabbit", sender: self)
            break
        case 3: practiceGame = "MatchGame"
            performSegue(withIdentifier: "match", sender: self)
            break
        case 4: practiceGame = "DropTypeGame"
            performSegue(withIdentifier: "rabbit", sender: self)
            break
        case 5: practiceGame = "DropSubGame"
            performSegue(withIdentifier: "rabbit", sender: self)
            break
        case 6: practiceGame = "ParenthesisGame"
            performSegue(withIdentifier: "rabbit", sender: self)
        default:
            break
            
    }
       
        

}
    //pop the view controller to go back
    @IBAction func backButtonActionButton(_ sender: Any) {
        Sound.play(file: "buttonclick.caf")
        guard (navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
        
    }
    //load the video passed
    func getVideo(videoCode: String){
        let url = URL(string: "https://www.youtube.com/embed/\(videoCode)")
        self.webview!.load(URLRequest(url: url!))
    }
    
    //sends the practice game string to the next view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "rabbit"){
            let controller = segue.destination as! ViewController
            controller.practiceGame = practiceGame
        }
        
    }

    
}
