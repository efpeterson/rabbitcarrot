//
//  DroppingMTMathUILabel.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 7/10/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import iosMath
import UIKit

class DroppingMTMathUILabel : MTMathUILabel {
    var answer: String?
}
