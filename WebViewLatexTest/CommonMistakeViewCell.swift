//
//  CommonMistakeViewCell.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/18/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit

class CommonMistakeViewCell: UICollectionViewCell{
    
    @IBOutlet var mistakePracticeImage: UIImageView!
    @IBOutlet var mistakePracticeLabel: UILabel!
    
    
    
    
}
