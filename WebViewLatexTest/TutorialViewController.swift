//
//  TutorialViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 8/8/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class TutorialViewController: UIViewController {
    @IBOutlet var tutorialVideoView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        guard (navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
    }

    private func playVideo() {
        guard let path = Bundle.main.path(forResource: "e sin tutorial", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
