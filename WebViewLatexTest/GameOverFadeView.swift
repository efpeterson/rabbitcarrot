//
//  GameOverFadeView.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 7/17/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class GameOverFadeView : UIView {
    init(frame: CGRect, msg: String) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 0,green: 0, blue: 0,alpha: 0)
        
        let label = UILabel()
        
        label.text = msg
        label.frame = frame
        label.textAlignment = .center
        label.textColor = UIColor.white
        addSubview(label)
        
        
        let button = UIButton()
        
        button.setBackgroundImage(UIImage(named: "menuButton1.png"), for: .normal)
        button.setTitle("Return to Menu", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.frame = CGRect(x: frame.width / 2 - 100,y: frame.height / 2 + 50, width: 200,height: 50)
        button.addTarget(self, action: #selector(self.returnToMenu), for: .touchUpInside)
        addSubview(button)
        
        UIView.animate(withDuration: 0.7, delay: 0.2, options: .curveEaseOut, animations: {
            
            self.backgroundColor = UIColor(red: 0,green: 0, blue: 0,alpha: 0.6)
            
        }, completion: { finished in
            
        })
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func returnToMenu(){
        
        guard (parentViewController?.navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
    }
}
