//
//  HomePageViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 6/13/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit
import iosMath

class HomePageViewController: UIViewController {

    @IBOutlet var continueButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Sound.enabled)
        Sound.play(file: "The Show Must Be Go", fileExtension: "caf", numberOfLoops: -1)
        //Sound.play(file: "rabbitCarrot", fileExtension: "caf", numberOfLoops: -1)
        // Do any additional setup after loading the view.
       
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func continueButtonAction(_ sender: Any) {
        
        performSegue(withIdentifier: "MainMenu", sender: self)
        
        //let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //let mainMenu = storyboard.instantiateViewController(withIdentifier: "MainMenuViewController") as! MainMenuViewController
        //self.present(mainMenu, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
