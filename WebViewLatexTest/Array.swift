//
//  Array.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/12/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation

extension Array{
    
    public mutating func shuffle(){
        
        for i in stride(from: count - 1, to: 1, by: -1){
            let random = Int(arc4random_uniform(UInt32(i + 1)))
            if i != random{
                swap(&self[i], &self[random])
            }
        }
        
    }
}
