//
//  ViewController.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 4/4/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
// 

import UIKit
import iosMath

class ViewController: UIViewController{

    var nestingDollview: NestingDollView!
    var practiceGame = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Chooses the view based on the game type as a string
        setScaleWidth(w: Int(self.view.frame.width))
        switch(practiceGame){
        case "RabbitGame":
            let gameView = RabbitView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), controller: self)
            self.view.addSubview(gameView)
            break
        case "DollGame":
            nestingDollview = NestingDollView(frame: self.view.frame)
            self.view.addSubview(nestingDollview)
            break;
        case "MatchGame":
            break;
        case "DropTypeGame":
            self.view.addSubview(DropTypeView(frame: self.view.frame, gt: 0))
            break;
        case "DropSubGame":
            self.view.addSubview(DropTypeView(frame: self.view.frame, gt: 1))
            break;
        case "ParenthesisGame":
            self.view.addSubview(ParenthesisPlacerView(frame: self.view.frame))
            break
        default:
            break
        }
        
    }
    func backButtonActionButton(_ sender: Any) {
        guard (navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
        
    }




    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

