//
//  CardDeck.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/12/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation

class CardDeck {
    
    //static var shared = CardDeck()
    var backImage = String()
    var formula: [String]
    var card1: Int
    var card2: Int
    
    init() {
        backImage = "card1"
        
        formula = ["Formula_1","Formula_2","Formula_3","Formula_4","Formula_5","Formula_6","Formula_7","Formula_8","Formula_9","Formula_10","Formula_11","Formula_12","Formula_13","Formula_14","Formula_15","Formula_16","Formula_17","Formula_18","Formula_19","Formula_20",]
        
        card1 = 0
        card2 = 0
    }
    
    func getCardBack() -> String{
        
        return backImage + ".png"
    }
    
    func getFormula(index: Int) -> String{
        
        return formula[index] + ".png"
    }
    
    func cardValue(index: Int) -> Int{
       
        let str = formula[index]
        let intString = str.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        return Int(intString)!
        
    }
    
    func randomizeCards(){
        
        formula.shuffle()
        
    }
    
    func isMatched(firstCard: Int, secondCard: Int) -> Bool{
        
        if firstCard + secondCard == 21{
            return true
        }else{
            return false
        }
        
    }
    
    
    
    
}
