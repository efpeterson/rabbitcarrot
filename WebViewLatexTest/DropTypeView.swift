//
//  DropTypeView.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 7/10/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class DropTypeView: UIView {
    var gameType = 0
    
    let settingImage = UIImage(named: "settings1x.png")
    var bg: UIImageView!
    var scoreCounter: UILabel!
    var buildingString = ""
    var buildingStringLabel: MTMathUILabel!
    var fallingQuestions: NSMutableArray!
    var spawnTimer: Timer?
    var gameOver = false
    
    init(frame: CGRect, gt: Int){
        self.gameType = gt
        let toolboxPosition = cgScale(467)
        let toolboxSize = frame.width - toolboxPosition
        super.init(frame: frame)
        self.backgroundColor = UIColor(colorLiteralRed: 0.59, green: 0.74, blue: 1, alpha: 1)
        bg = UIImageView()
        bg.image = UIImage(named:(gt != 0  ? "dropSunset1.png" : "dropScene1.png"))
        bg.frame = CGRect(x: 0, y: 0, width: cgScale( 467), height: frame.height)
        
        addSubview(bg)
        
        fallingQuestions = NSMutableArray()
        
        let scoreDisplay = UIImageView()
        scoreDisplay.image = UIImage(named: "liveCounter1.png")
        scoreDisplay.frame = CGRect(x: 0, y: 0, width: scale( 75), height: scale( 25))
        self.addSubview(scoreDisplay)
        
        scoreCounter = UILabel()
        scoreCounter.text = "10"
        scoreCounter.textColor = UIColor.black
        scoreCounter.frame = CGRect(x: scale(10), y: 0, width: scale( 50), height: scale( 25))
        self.addSubview(scoreCounter)
        
        
        
        let toolbox = UIImageView()
        toolbox.image = UIImage(named: "toolbox.png")
        toolbox.frame = CGRect(x: cgScale( 467), y: 0, width: frame.width - cgScale( 467), height: frame.height)
        
        self.addSubview(toolbox)
        
        let menuButton = UIButton()
        menuButton.setTitle("settings", for: .normal)
        menuButton.frame = CGRect(x: 0, y: super.bounds.maxY - 30, width: 30, height: 30)
        menuButton.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        menuButton.setImage(settingImage, for: .normal)
        self.addSubview(menuButton)
        
        //create numpad
        let microbuttonSize = toolboxSize / 3
        for i in 0...11 {
            let num = (i + 1) % 10
            let microButton = UIButton()
            microButton.frame = CGRect(x: toolboxPosition + CGFloat(i % 3) * microbuttonSize, y: CGFloat(i / 3) * microbuttonSize, width: microbuttonSize, height: microbuttonSize)
            microButton.addTarget(self, action: #selector(self.microButtonPressed), for: .touchUpInside)
            microButton.setBackgroundImage(UIImage(named: "microButton1.png"), for: .normal)
            
            if( i == 10 ) {
                microButton.setTitle("-", for: .normal)
                
            } else if (i == 11) {
                microButton.setTitle("/", for: .normal)
            } else {
                microButton.setTitle("\(num)", for: .normal)
            }
            microButton.setTitleColor(UIColor.black, for: .normal)
            self.addSubview(microButton)
        }
        let clearButton = UIButton()
        clearButton.frame = CGRect(x: toolboxPosition, y: microbuttonSize * 4, width: toolboxSize, height: microbuttonSize)
        clearButton.setTitle("Clear Entry", for: .normal)
        clearButton.addTarget(self, action: #selector(self.clearEntry), for: .touchUpInside)
        addSubview(clearButton)
        
        buildingStringLabel = MTMathUILabel()
        buildingStringLabel.frame = CGRect(x: toolboxPosition, y: microbuttonSize * 5, width: toolboxSize, height: self.frame.height - microbuttonSize * 5.3)
        buildingStringLabel.textAlignment = .center
        updateBuildingLabel()
        addSubview(buildingStringLabel)
        
        if(gt == 0) {
            loadRandomQuestion()
            spawnTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.loadRandomQuestion), userInfo: nil, repeats: true)
        } else {
            loadRandomSubQuestion()
            spawnTimer = Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(self.loadRandomSubQuestion), userInfo: nil, repeats: true)
        }
        
        
        
    }
    func microButtonPressed(pressed: UIButton){
//        print(pressed.title(for: .normal))
        if(gameOver) {
            return
        }
        buildingString.append(pressed.title(for: .normal)!)
        updateBuildingLabel()
        checkForAnswerMatches()
    }
    func clearEntry(){
        buildingString = ""
        updateBuildingLabel()

    }
    func updateBuildingLabel(){
        buildingStringLabel.latex = "x^{\(buildingString)}"
        maximizeFontSize(label: buildingStringLabel)
    }
    func checkForAnswerMatches() {
        var i = 0
        while( i < fallingQuestions.count){
            let question = fallingQuestions.object(at: i)
            let q = question as! DroppingMTMathUILabel
            if q.answer == buildingString {
                let smoke = SmokeEffect()
                
                smoke.frame = CGRect(x: 0, y: 0, width: q.frame.width, height: q.frame.height)
                q.latex = ""
                self.fallingQuestions.remove(question)
                //q.removeFromSuperview()
                q.addSubview(smoke)
                Sound.play(file: "Explosion", fileExtension: "caf", numberOfLoops: 0)
                smoke.onDestroy = {
                    q.removeFromSuperview()
                }
                pauseAnimation(layer: q.layer)
                q.backgroundColor = UIColor.clear
                Timer.scheduledTimer(timeInterval: 0.5, target: self,selector: #selector(self.clearEntry), userInfo: nil, repeats: false)
                
                
            }
            i += 1
        }
    }
    func loadRandomQuestion() -> DroppingMTMathUILabel{
        var a = getRandom(range: 9) + 1
        var b = getRandom(range: 8) + 2
        //if it simplifies wayyy to easily, try again
        if(a % b == 0){
            return loadRandomQuestion()
        }
        //simplify if possible
        let g = gcd(a: a, b: b)
        a /= g
        b /= g
        
        
        
        var style = getRandom(range: 3)
        if(b == a) {
            style = 1
            
        }
        var latex = ""
        var answer = ""
        let u = "x"
        switch(style){
        case 0:
            if(a == 1) {
                latex = "\\sqrt\(b == 2 ? "" : "[\(b)]"){\(u)}"
            } else {
                latex = "\\sqrt\(b == 2 ? "" : "[\(b)]"){\(u)^{\(a)}}"
            }
            answer = "\(a)/\(b)"
            break
        case 1:
            if(a == 1){
                latex = "\\frac{1}{\(u)}"
            } else {
                latex = "\\frac{1}{\(u)^{\(a)}}"
            }
            answer = "-\(a)"
            break
        case 2:
            if(a == 1){
                latex = "\\frac{1}{\\sqrt\(b == 2 ? "" : "[\(b)]"){\(u)}}"
            } else {
                latex = "\\frac{1}{\\sqrt\(b == 2 ? "" : "[\(b)]"){\(u)^{\(a)}}}"
            }
            answer = "-\(a)/\(b)"
            break;
        default:
            break
        }
        let newQuestion = DroppingMTMathUILabel()
        let size = 75
        let xPosition = size / 2 + getRandom(range: scale(467 - (3 * size / 2)))
        
        newQuestion.frame = CGRect(x: xPosition, y: -size, width: size, height: size)
        newQuestion.latex = latex
        newQuestion.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
        newQuestion.textAlignment = .center
        newQuestion.answer = answer
        print(answer)
        maximizeFontSize(label: newQuestion)
        
        UIView.animate(withDuration: 15.0, delay: 0.0, options: .curveLinear, animations: {
            
            
            var topFrame = newQuestion.frame
            topFrame.origin.y += self.frame.height
            
            newQuestion.frame = topFrame
        }, completion: { finished in
            newQuestion.removeFromSuperview()
            if(finished) {
                self.onQuestionDropped()
            }
        })
        
        addSubview(newQuestion)
        fallingQuestions.add(newQuestion)
        return newQuestion
        
    }
    func loadRandomSubQuestion() -> DroppingMTMathUILabel{
        var a = getRandom(range: 9) + 1
        var b = getRandom(range: 8) + 2
        //simplify if possible
        let g = gcd(a: a, b: b)
        a /= g
        b /= g
    
        var c = getRandom(range: 9) + 1
        var d = getRandom(range: 8) + 2
    
        let h = gcd(a: c, b: d )
        
        c/=h
        d/=h
    
        var e = a*d-b*c
        var f = b*d
        
        let i = gcd(a: abs(e), b: f)
        
        e/=i
        f/=i
        
        
        
        let latex = "\(a  == b ?  "1" : (b == 1 ? "\(a)" : "\\frac{\(a)}{\(b)}")) - \(c == d ? "1" : (d == 1 ? "\(c)" : "\\frac{\(c)}{\(d)}"))"
        let answer = "\(e)\(f != 1 ? "/\(f)" : "")"
        let newQuestion = DroppingMTMathUILabel()
        let size = 75
        let xPosition = size / 2 + getRandom(range: scale(467 - (3 * size / 2)))
        
        newQuestion.frame = CGRect(x: xPosition, y: -size, width: size, height: size)
        newQuestion.latex = latex
        newQuestion.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
        newQuestion.textAlignment = .center
        newQuestion.answer = answer
        print(answer)
        maximizeFontSize(label: newQuestion)
        
        UIView.animate(withDuration: 25.0, delay: 0.0, options: .curveLinear, animations: {
            
            
            var topFrame = newQuestion.frame
            topFrame.origin.y += self.frame.height
            
            newQuestion.frame = topFrame
        }, completion: { finished in
            newQuestion.removeFromSuperview()
            if(finished) {
                self.onQuestionDropped()
            }
        })
        
        addSubview(newQuestion)
        fallingQuestions.add(newQuestion)
        return newQuestion
            

    }
    func pauseAnimation(layer: CALayer) {
        let pausedTime: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    func onQuestionDropped() {
        if(gameOver) {
            return
        }
        let value = Int(scoreCounter.text!)
        if(value == 1) {
            //game over
            addSubview(GameOverFadeView(frame: frame, msg: "Game Over"))
            gameOver = true
            
            spawnTimer?.invalidate()
            for j in fallingQuestions {
                let f = j as! DroppingMTMathUILabel
                f.layer.timeOffset = f.layer.convertTime(CACurrentMediaTime(), from: nil)
                f.layer.beginTime = CACurrentMediaTime();
                f.layer.speed = 5.0;
            }
            
            
        }
        scoreCounter.text = "\(value! - 1)"

    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
