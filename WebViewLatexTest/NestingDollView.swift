//
//  NestingDollView.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/27/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

class NestingDollView: UIView {
    
    var movingFormula: MTMathUILabel? //the formula being dragged. Is nil when nothing is being dragged
    var formulas: NSMutableArray! //an array that stores the formula
    let settingImage = UIImage(named: "settings1x.png")
    var scoreCounter: UILabel!
    var dolls: Stack<NestingDoll>! //stores all the dolls
    var answerDisplay: Stack<MTMathUILabel>!
    var answerDolls: Stack<NestingDoll>!
    var questionBank: NSMutableArray!
    var questionsAnswered: NSMutableArray!
    var currentQuestionIndex = -1
    
    var question: MTMathUILabel?
    var sequenceEnd = 0
    var sequencePoint = 0
    var bg: UIImageView!
    var openDoll: NestingDoll?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.backgroundColor = UIColor(colorLiteralRed: 0.59, green: 0.74, blue: 1, alpha: 1)
        dolls = Stack<NestingDoll>()
        answerDisplay = Stack<MTMathUILabel>()
        answerDolls = Stack<NestingDoll>()
        formulas = NSMutableArray()
        questionBank = NSMutableArray()
        questionsAnswered = NSMutableArray()
        bg = UIImageView()
        bg.image = UIImage(named:"dollBG1.png")
        bg.frame = CGRect(x: 0, y: 0, width: cgScale( 467), height: frame.height)
        
       // addSubview(bg)
        
        let scoreDisplay = UIImageView()
        scoreDisplay.image = UIImage(named: "dollScore1.png")
        scoreDisplay.frame = CGRect(x: 0, y: 0, width: scale( 75), height: scale( 25))
        self.addSubview(scoreDisplay)
        
        scoreCounter = UILabel()
        scoreCounter.text = "0"
        scoreCounter.textColor = UIColor.black
        scoreCounter.frame = CGRect(x: scale(10), y: 0, width: scale( 50), height: scale( 25))
        self.addSubview(scoreCounter)
        
        
        
        let toolbox = UIImageView()
        toolbox.image = UIImage(named: "toolbox.png")
        toolbox.frame = CGRect(x: cgScale( 467), y: 0, width: frame.width - cgScale( 467), height: frame.height)
        
        self.addSubview(toolbox)
        
        let menuButton = UIButton()
        menuButton.setTitle("settings", for: .normal)
        menuButton.frame = CGRect(x: 0, y: super.bounds.maxY - 30, width: 30, height: 30)
        menuButton.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        menuButton.setImage(settingImage, for: .normal)
        self.addSubview(menuButton)
        
        questionBank = readJSONDatabase(db: "nesting")
        loadRandomQuestion()
    }
    /*
     Dragging logic, if movingFormula is not null, move that UIWebView to the current touch location
     */
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first{
            if let toMove = movingFormula {
                let touchLocation = touch.location(in: self)
                toMove.center.y = touchLocation.y
                toMove.center.x = touchLocation.x
            }
        }
    }
    /*
     Handles the beginning of touch events, detects if the touch is inside any formula view and if so, sets them to the movingFormula variable for dragging logic
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first{
            let touchLocation = touch.location(in: self)
            print(touchLocation)
            for formula in formulas{
                let formulaView = formula as! MTMathUILabel
                if(formulaView.frame.contains(touchLocation)){
                    self.movingFormula = formulaView;
                    movingFormula?.backgroundColor = UIColor(colorLiteralRed: 0.8, green: 0.8, blue: 1, alpha: 0.5)
                    break;
                }
            }
        }
    }
    /*
     Detects whether a touch release point of dragging a formula is within the bounds of a rabbit
     */
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (movingFormula != nil) {
            //Reset the answer's formula view
            let initalPosition = movingFormula?.layer.value(forKey: "initialPosition") as! CGPoint
            movingFormula?.center = initalPosition
            movingFormula?.backgroundColor = UIColor.clear
            
            let touchLoc = touches.first?.location(in: self)
            
            let doll = dolls.peek();
            if(doll?.frame.contains(touchLoc!))! {
                let seq = movingFormula?.layer.value(forKey: "sequence")
                if seq != nil {
                    let val = seq as! Int
                    if(val == sequencePoint + 1) {
                        //correct answer was chosen
                        sequencePoint += 1
                        doll?.setFormula(form: (movingFormula?.latex)!)
                        
                        
                        if(SettingsManager.soundOn) {
                            Sound.play(file: "success.caf")
                        }
                        
                        if(sequencePoint == sequenceEnd) {
                            
                            doFinalAnimation()
                            
                        } else {
                            dolls.push(item: openDoll!)
                            openDoll?.animateClose(nil)
                            openDoll = spawnLargerDoll()
                        }
                    } else {
                        //wrong answer
                        if(SettingsManager.soundOn) {
                            Sound.play(file: "error.caf")
                        }
                    }
                }else {
                    //wrong answer
                    if(SettingsManager.soundOn) {
                        Sound.play(file: "error.caf")
                    }
                }
            }
            movingFormula = nil
        } else {
            //not moving a formula
        }
    }
    func doFinalAnimation(){
        if let doll = dolls.pop() {
           

            doll.animateOpen({
                let newView = MTMathUILabel()
                newView.fontSize = 30
                newView.latex = doll.formula?.latex
                newView.frame = CGRect(x: 0, y: (doll.formula?.frame.maxY)!, width: (doll.formula?.frame.width)!, height: (doll.formula?.frame.height)!)
                
               // let dollScale = cgScale(75) * CGFloat(pow(0.9, self.answerDisplay.count()).doubleValue)
               
                let smallDoll = NestingDoll(frame: doll.frame)
                doll.copyDataTo(newDoll: smallDoll)
                smallDoll.setToDisplayPosition()
                
                self.addSubview(smallDoll)
                self.answerDolls.push(item: smallDoll)
               // self.answerDisplay.push(item: newView)
               // self.addSubview(newView)
                doll.removeFromSuperview()
                
                self.doFinalAnimation()
            })
        } else {
            self.scoreCounter.text = String(Int(self.scoreCounter.text!)! + 1)
            Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.endQuestionAnim), userInfo: nil, repeats: false)
        }
        
    }
    func endQuestionAnim() {
        self.questionsAnswered.add(self.currentQuestionIndex)
        self.loadRandomQuestion()
    }
    func spawnLargerDoll() -> NestingDoll{
        let scale = 1.1;
        let basicSize = 60
        let newSize = CGFloat(pow(Decimal(scale), dolls.count() + 2).doubleValue * Double(basicSize))
        let ndTest = NestingDoll(frame: CGRect(x: cgScale( 233) - newSize / 2, y: 0, width:  newSize,height: frame.height * 0.75))
        ndTest.index = dolls.count()
        insertSubview(ndTest, aboveSubview: (dolls.peek() != nil ? dolls.peek() : bg)!)
        //dolls.push(item: ndTest)
        return ndTest;
    }
    /*
     picks a random question from the question bank and loads all the formulaviews for it
     
     */
    func loadRandomQuestion() {
        while(dolls.peek() != nil){
            dolls.pop()?.removeFromSuperview()
        }
        while(answerDolls.peek() != nil){
            answerDolls.pop()?.removeFromSuperview()
        }
        while(answerDisplay.peek() != nil){
            answerDisplay.pop()?.removeFromSuperview()
        }
        if( openDoll != nil ) {
            openDoll?.removeFromSuperview()
        }
        let firstDoll = spawnLargerDoll()
        firstDoll.animateClose(nil)
        dolls.push(item: firstDoll)
        openDoll = spawnLargerDoll()
        for f in formulas {
            let formula = f as! MTMathUILabel
            formula.removeFromSuperview()
        }
        formulas.removeAllObjects()
        if(self.question != nil) {
            self.question!.removeFromSuperview()
        }
        sequencePoint = 0
        if questionsAnswered.count == questionBank.count {
            //all questions have been answered correctly
            
//            gameOverLabel.text = "You win!"
//            gameOverLabel.sizeToFit()
//            gameOverLabel.center = CGPoint(x: gardenBG.frame.width / 2, y: self.frame.height / 2)
//            timer?.invalidate()
//            hopAnimationTimer?.invalidate()
//            self.addSubview(gameOverLabel)
//            self.addSubview(gameOverButton)
            
            addSubview(GameOverFadeView(frame: frame, msg: "You win"))
            
            return
        }
        
        var questionIndex = -1
        repeat {
            questionIndex = getRandom(range: questionBank.count)
            
        } while(hasAnsweredQuestion(index: questionIndex))
        
        
        let questionDict = questionBank.object(at: questionIndex) as! NSDictionary
        currentQuestionIndex = questionIndex
        
        //calculate how large the button can be
        let max = questionDict.count - 3;
        let buttonHeight = CGFloat(self.frame.height / CGFloat(max + 1));
        let sequence = (questionDict.value(forKey: "sequence")) as! String
        let seqExploded = sequence.components(separatedBy: ",")
        sequenceEnd = seqExploded.count
        //create buttons for each of the different listed formulas
        for i in 0...max{
            let formula = (questionDict.value(forKey: "comp" + String(i + 1)) as! String)
           // let isAnswer = (questionDict.value(forKey: "correctAnswer") as! String)  == (("answer" + String(i + 1)))
            let f = createFormulaView(x: self.frame.width - cgScale( 100), y: CGFloat( buttonHeight / 2 + (buttonHeight * CGFloat(i))), formula: formula, width: cgScale( 190), height: buttonHeight, textColor: UIColor.black, isAnswer: false)
            for j in 0...(seqExploded.count - 1) {
                if(String(i + 1) == seqExploded[j]) {
                    f.layer.setValue(j + 1, forKey: "sequence")
                }
            }
            
            
            
            formulas.add(f)
            self.addSubview(f)
        }
        
        let question = questionDict.value(forKey: "question") as! String
        //create the incorrect formulaView to connect to the rabbit
        self.question = createFormulaView(x: 0, y: 0, formula: question, width: cgScale( 200), height: cgScale( 50), textColor: UIColor.black,isAnswer: false)
        self.question?.frame = CGRect(x: cgScale(233) - cgScale( 200) / 2, y: 0, width: cgScale( 200), height: cgScale( 50))
        addSubview(self.question!)
        unifyFormulaFontSizes(formulas: formulas)
        
    }
    func hasAnsweredQuestion(index: Int) -> Bool{
        for j in questionsAnswered {
            if (j as! Int) == index {
                return true
            }
        }
        return false;
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
