//
//  SmokeEffect.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/21/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit

class SmokeEffect: UIImageView {
    var onDestroy = {}
    init() {
        super.init(image: UIImage(named: "smoke1.png"))
        Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.deleteMe), userInfo: nil, repeats: false)
    }
    required init?(coder: NSCoder){
        super.init(coder: coder)
    }
    func deleteMe() {
        onDestroy()
        removeFromSuperview()
        
    }
    
}
