//
//  NestingDollViewController.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/27/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit
import iosMath

class NestingDollViewController: UIViewController {
    var nestingDollview: NestingDollView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setScaleWidth(w: Int(self.view.frame.width))
        nestingDollview = NestingDollView(frame: self.view.frame)
        self.view.addSubview(nestingDollview)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
