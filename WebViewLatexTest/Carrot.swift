//
//  Carrot.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/20/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit

class Carrot: UIImageView {
    let carrotHealthTimer = 60.0
    var owner: UIView!
    var health = 60.0
    init(owner: UIView) {
        super.init(image: UIImage(named: "carrot.png"))
        self.owner = owner;
        
    }
    required init?(coder: NSCoder){
        super.init(coder: coder)
    }
    public func resetHealthTimer() {
        health = carrotHealthTimer;
    }
    public func update(delta: Double) {
        
        let curHealth = health / carrotHealthTimer
        if(curHealth < 0) {
            //target destroyed
            image = UIImage(named: "bite4-1.png")
        } else if(curHealth < 0.25){
            image = UIImage(named: "bite3-1.png")
        } else if(curHealth < 0.50){
            image = UIImage(named: "bite2-1.png")
        } else if(curHealth < 0.75){
            image = UIImage(named: "bite1-1.png")
        } else {
            image = UIImage(named: "carrot.png")
        }
    }
}
