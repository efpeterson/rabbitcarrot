//
//  GameOverViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 7/16/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit

class GameOverViewController: UIViewController {

    @IBOutlet var gameOverView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        gameOverView.layer.cornerRadius = 10
        gameOverView.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func endGame(_ sender: Any) {
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
