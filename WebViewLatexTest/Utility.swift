//
//  Utility.swift
//  WebViewLatexTest
//
//  Created by Peterson, Edward F on 6/27/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import Foundation
import UIKit
import iosMath

var scaleWidth: Int!

func setScaleWidth(w: Int) {
    scaleWidth = w
}

/*
 Returns a random integer value from 0 (inclusive) to range (exclusive)
 */
func getRandom(range: Int) ->Int{
    return Int(arc4random_uniform(UInt32(range)))
}
/*
 scaling function for 16 * 9 screens
 */
func scale(_ v: Int) ->Int{
    return Int(Float(Double(v) / 667.0) * Float(scaleWidth))
}
/*
 CGFloat casted scaling function
 */
func cgScale(_ v: Int) ->CGFloat{
    return CGFloat(scale(v))
}
/*
 Reads the JSON file and stores each question into the question bank as an NSDictionary
 */
func readJSONDatabase(db: String) ->NSMutableArray{
    let questionBank = NSMutableArray()
    do {
        if let file = Bundle.main.url(forResource: db, withExtension: "json") {
            let data = try Data(contentsOf: file)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            if let object = json as? [Any] {
                // json is an array
                for entry in object {
                    let dict = entry as! NSDictionary
                    questionBank.add(dict)
                    //                        print(dict.value(forKey: "question"))
                }
                
                
            } else {
                print("JSON is invalid")
            }
        } else {
            print("no file")
        }
    } catch {
        print(error.localizedDescription)
    }
    return questionBank
}
/*
 Creates the UIWebView and sets all the properties needed
 */
func createFormulaView(x: CGFloat, y: CGFloat, formula: String, width: CGFloat, height: CGFloat, textColor: UIColor, isAnswer: Bool) -> MTMathUILabel{
    
    
    let formulaView = MTMathUILabel(frame: CGRect(x: 0, y: 0, width: width, height: height))
    formulaView.center = CGPoint(x: x - width / 2, y: y - height / 2)
    formulaView.textAlignment = .center
    formulaView.latex = formula
    formulaView.frame = CGRect(x: x - width / 2, y: y - height / 2, width: width, height: height)
    maximizeFontSize(label: formulaView)
    
    //sets the last of the info for the view
    
    
    formulaView.textColor = textColor
    formulaView.layer.setValue(CGPoint(x: x, y: y), forKey: "initialPosition")
    formulaView.layer.setValue(isAnswer, forKey: "isAnswer")
//    if draggable {
//        
//        formulas.add(formulaView)
//    }
    
    return formulaView
    
}
func maximizeFontSize(label: MTMathUILabel){
    let desiredSize = CGSize(width: label.frame.width, height: label.frame.height)
    
    //reads the current font size of the formula
    let bf = Float(label.fontSize)
    //calculate the new font size scale
    label.frame.size.height = 1
    label.frame.size.width = 1
    let badSize = label.sizeThatFits(CGSize.zero)
    
    let nWidth = (bf * Float(desiredSize.width)) / Float(badSize.width);
    let nHeight = (bf * Float(desiredSize.height)) / Float(badSize.height);
    //this scales the font to be slightly smaller to fit better in the bounds
    let newFontSize = min(nWidth, nHeight) * 0.95;
    
    //scale the font to the new size
    label.fontSize = CGFloat(newFontSize)
    label.frame.size  = desiredSize
}
func unifyFormulaFontSizes(formulas: NSMutableArray){
    var smallestFont = 100000
    for j in formulas {
        let f = j as! MTMathUILabel
        smallestFont = min(smallestFont, Int(f.fontSize))
    }
    for j in formulas {
        let f = j as! MTMathUILabel
        f.fontSize = CGFloat(smallestFont)
        
    }
}
func getFlippedImage(image: UIImage) ->UIImage {
    return UIImage(cgImage: image.cgImage!, scale: 1, orientation: UIImageOrientation.upMirrored)
}
//Used for timing loop
func getCurrentMillis()->Int64 {
    return Int64(Date().timeIntervalSince1970 * 1000)
}
/*
 Returns a random boolean value
 */
func randomBool() -> Bool {
    return arc4random_uniform(2) == 0
}
extension Decimal {
    var doubleValue:Double {
        return NSDecimalNumber(decimal:self).doubleValue
    }
}
func gcd(a:Int, b:Int) -> Int {
    if a == 1 || b == 1 {
        return 1
    }
    if a == b {
        return a
    }
    else {
        if a > b {
            return gcd(a:a-b,b:b)
        }
        else {
            return gcd(a:a,b:b-a)
        }
    }
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
