//
//  MainMenuViewController.swift
//  WebViewLatexTest
//
//  Created by Rakesh Joshi on 6/13/17.
//  Copyright © 2017 Toasted Pixel. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {

    //IBOutlets for all the buttons
    @IBOutlet var tutorialButton: UIButton!
    @IBOutlet var commonMistakesButton: UIButton!
    @IBOutlet var playGameButton: UIButton!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var OptionsButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Sound.enabled)
        //Sound.play(file: "The Show Must Be Go", fileExtension: "caf", numberOfLoops: -1)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func backButtonAction(_ sender: Any) {
        guard (navigationController?.popViewController(animated: true)) != nil
            else {
                print("No Navigation Controller")
                return
        }
        
    }

    @IBAction func commonMistakesButtonAction(_ sender: Any) {
        Sound.play(file: "buttonclick.caf")
        performSegue(withIdentifier: "commonNew", sender: self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func tutorialAction(_ sender: Any) {
        performSegue(withIdentifier: "tutorial", sender: self)
    }
}
